private namespace alice.spain.landcoverchange;

model 'local:stefano.balbi:im.alice.spain:current_cover_2019_withFire_50m_wgs84'
	as landcover:LandCoverType 
	       classified into
           landcover:BroadleafForest        if 1,
		   landcover:ConiferousForest       if 2,
	       landcover:Shrubland              if 3,  
	       landcover:Grassland              if 5,  //avoided pastureland as it is under agri vegetation
	       landcover:AgriculturalVegetation if 6,  
           landcover:BareArea               if 7,  	      
	       landcover:ArtificialSurface      if 8, 
	       landcover:WaterBody              if 9
	       ;


private learn landcover:LandCoverType
	observing
		@predictor distance to infrastructure:Highway,
		@predictor distance to earth:Waterway,
		@predictor distance to earth:Coastline,
		@predictor distance to conservation:ProtectedArea,
		@predictor geography:Slope,
		@predictor geography:Elevation,
		@predictor geography:Aspect,
		@predictor soil.incubation:SoilDepth

	using im.weka.bayesnet(resource = suitability.bn.alice.ws); 	
	
@time(step=1.year)
private model change in landcover:LandCoverType
	observing
		distance to infrastructure:Highway,
		distance to earth:Waterway,
		distance to earth:Coastline,
		distance to conservation:ProtectedArea,
		geography:Slope,
		geography:Elevation,
		geography:Aspect,
		soil.incubation:SoilDepth
	
	using klab.landcover.allocate(
		suitability = 'luc.suitability.bn.pma50',
		greedy= false,
		resistances = {
			landcover:BroadleafForest:  	  0.93,
			landcover:ConiferousForest: 	  0.93, 
			landcover:Shrubland:        	  0.93,
			landcover:Grassland:         	  0.93,
			landcover:AgriculturalVegetation: 0.93,
			landcover:BareArea: 			  0.93,
			landcover:ArtificialSurface:      0.93,
			landcover:WaterBody: 				1		
	      	},
		demand = {
			landcover:ArtificialSurface: 0.02
					},

		transitions = {{
			       *                        | landcover:BroadleafForest | landcover:ConiferousForest  | landcover:Shrubland | landcover:Grassland |landcover:AgriculturalVegetation| landcover:BareArea | landcover:ArtificialSurface | landcover:WaterBody,
			landcover:BroadleafForest       | 0                      	| 0                        	  | 0                	| 0                	  | 0                              | 0               	| 0                      	  |  0             	   ,
			landcover:ConiferousForest      | 0                      	| 0                        	  | 0                	| 0                	  | 0                              | 0               	| 0                      	  |  0                 ,
		    landcover:Shrubland             | 0.1                      	| 0.1                      	  | 0               	| 0                   | 0                              | 0               	| 0                      	  |  0                 ,  
	        landcover:Grassland             | 0.2                      	| 0.5                         | 0.2                	| 0                	  | 0                              | 0               	| 0.1                         |  0                 , 
	        landcover:AgriculturalVegetation| 0.1                    	| 0.1                         | 0.2                	| 0.5                 | 0                              | 0               	| 0.1                         |  0                 , 
            landcover:BareArea              | 0.9                  		| 0.9                         | 0.8                	| 0                   | 0                              | 0               	| 0                           |  0                 ,       
	        landcover:ArtificialSurface     | 0                     	| 0                           | 0                	| 0                	  | 0                              | 0               	| 0                           |  0                 ,
	        landcover:WaterBody             | 0                    	    | 0                           | 0                	| 0                	  | 0                              | 0               	| 0                           |  0             
	        }}
	);


